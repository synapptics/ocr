from pymongo import MongoClient

class Conexao(object):
    
    @staticmethod
    def client():
        return MongoClient('mongodb://localhost:27017/')