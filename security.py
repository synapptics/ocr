import hashlib

class Security:


    @staticmethod
    def validarToken(token, publicKey):
        pk = str(publicKey)
        _token = hashlib.md5('xYvS[NDnc/)>jk[B/,rj,]=?Mn6(&)4=A-vgtPkmw<@x=.P3TV' + pk).hexdigest()
        print 'PK: ' + pk + ', TOKEN: ' + _token
        return token == _token
