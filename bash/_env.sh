#!/bin/bash

sudo echo "[*] Atualizando pacotes..."
sudo yum -y update >> env.log.txt


sudo echo "[*] Instalando Development Tools..."
sudo yum -y group install "Development Tools"  >> env.log.txt

sudo yum -y install python-devel python-nose python-setuptools gcc gcc-gfortran gcc-c++ blas-devel lapack-devel atlas-devel  >> env.log.txt

sudo echo "[*] Instalando git..."
sudo yum -y install git   >> env.log.txt

sudo echo "[*] Instalando pip..."
sudo wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py  >> env.log.txt
sudo rm get-pip.py 

sudo echo "[*] Instalando opencv+numpy..."
sudo yum -y install numpy  >> env.log.txt
sudo yum -y install opencv >> env.log.txt

sudo echo "[*] Instalando pacotes... "
sudo pip install flask --upgrade >> env.log.txt
sudo pip install pillow --upgrade >> env.log.txt
sudo pip install imutils --upgrade >> env.log.txt
sudo pip install pytesseract --upgrade >> env.log.txt
sudo pip install bson --upgrade >> env.log.txt
sudo pip install pymongo --upgrade >> env.log.txt
sudo pip install http --upgrade >> env.log.txt

sudo su -c "yum-builddep python-matplotlib"
sudo pip install matplotlib --upgrade  >> env.log.txt

sudo echo "[*] Instalando Tesseract..."
sudo yum -y install tesseract tesseract-en ImageMagick >> env.log.txt


sudo echo "[*] Instalando mongodb..."
sudo rm /etc/yum.repos.d/mongodb.repo
sudo echo "[mongodb]" >> /etc/yum.repos.d/mongodb.repo
sudo echo "name=MongoDB Repository" >> /etc/yum.repos.d/mongodb.repo
sudo echo "baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/" >> /etc/yum.repos.d/mongodb.repo
sudo echo "gpgcheck=0" >> /etc/yum.repos.d/mongodb.repo
sudo echo "enabled=1" >> /etc/yum.repos.d/mongodb.repo
sudo yum -y update  >> env.log.txt
sudo yum -y install mongodb-org mongodb-org-server >> env.log.txt
sudo mkdir /data/
sudo mkdir /data/db
sudo chmod -R 777 /data/db
sudo systemctl start mongod >> env.log.txt

sudo echo "[*] Habilitando regras em iptables..."
sudo iptables -I INPUT -p tcp --dport 5000 -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 27017 -j ACCEPT
sudo iptables -I INPUT -p tcp --dport 80 -j ACCEPT


echo "    **** finalizado ****"