import ocr 
import json
import cStringIO
import time
import funcoes as Funcoes

import base64
import cStringIO

from security import Security
from flask import Flask, request, json
from processamento import Processamento
from random import randint
from bson import json_util
from definicoes import Definicoes
from gabarito import Gabarito
from resposta import Resposta
from io import BytesIO
from PIL import Image
import cv2

app = Flask(__name__)


'''
    Processa um novo cartao resposta
'''
@app.route('/v1/processar/<th>', methods=['POST'])
def index(th = '1'):

    token = request.form.get('token')
    publicKey = request.form.get('public_key')

    if not Security.validarToken(token, publicKey):
        pass
        # return app.response_class(
        #     response=json.dumps({ 'error': True , 'msg': 'Acesso nao autorizado' }),
        #     status=200,
        #     mimetype='application/json'
        # )


    rand1 = str(randint(1, 999999999999))
    rand2 = str(randint(1, 999999999999))

    random_filename = 'o_' + rand1 + '_' + rand2 + '.jpg'
    path = Definicoes.pathEntrada + random_filename
    

    img = request.files.get('imagem_entrada')
    #img = Image.open('entrada/teste.jpg')
    img.save(path)

    dados = {}
    dados['nome_usuario'] = request.form.get('nome_usuario')
    dados['codigo_escola'] = request.form.get('codigo_escola')
    dados['tipo_prova'] = request.form.get('tipo_prova')
    dados['manuscrito'] = False
    dados['presente'] = True    
    dados['entrada'] = Funcoes.encodeB64(path)

    

    if dados['tipo_prova'] == 'AUSENTE':
        dados['presente'] = False
    elif dados['tipo_prova'] == 'MANUSCRITO':
        dados['manuscrito'] = True

    
    processamento = Processamento()
    processamento.setPath(path)  
    processamento.setDados(dados)

    if th == '1':
        try:
            processamento.start()
            data = {
                'error': False,
                'msg': 'Processando'
            }

        except Exception as error:
            data = {
                'error': True,
                'msg': error[0]
            }
    else:

        img, idt, respostas = processamento.iniciarSemThread()
        data = { 'resposta':  respostas, 'identificador': idt, 'imagem': img }

    
    return app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
