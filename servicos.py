import requests
import json
from definicoes import Definicoes

class Servicos:

    @staticmethod
    def iniciarProcessamento(dados):
        header = {
            "content-type": "application/json"
        }

        jdados = {
            "escola": dados['codigo_escola'],
            "usuario": dados['nome_usuario'],
            "presente": dados['presente'],
            "manuscrito": dados['manuscrito'],
            "prova": "1",
            "cartao": dados['entrada']
        }


        req = requests.post(Definicoes.servicoNovoProcessamento,
                            data=json.dumps(jdados), headers=header)
 

        retorno = json.loads(req.text)
        if retorno['status'] == '0':
            raise Exception('Erro ao processar imagem no servidor SIAP #1')
        else:
            return retorno['codigo']




    @staticmethod
    def finalizarProcessamento(dados):
        header = {
            "content-type": "application/json"
        }

        req = requests.post(Definicoes.servicoProcessamentoFinalizado,
                            data=json.dumps(dados), headers=header)

        print "------ req2 -------"
        print req.text
        print "---x--"
        retorno = json.loads(req.text)
        if retorno['status'] == '0':
            raise Exception('Erro ao processar imagem no servidor SIAP #2')
