import datetime
import json
from bson.objectid import ObjectId
from mongo import Conexao


class Gabarito(object):

    def __init__(self, b64entrada, b64saida, identificador, marcacoes):
        self.client = Conexao.client()
        self.db = self.client['ocr_database']

        self.entrada = b64entrada
        self.saida = b64saida
        self.identificador = identificador
        self.marcacoes = marcacoes


    def salvar(self):   
        dados = {
            "data": datetime.datetime.utcnow(),
            "identificador": self.identificador,
            "marcacoes": self.marcacoes,
            "imagens": {
                "entrada": self.entrada,
                "saida": self.saida
            }
        }

        self.db.correcoes.insert_one(dados)


    @staticmethod
    def find(_id):
        cliente = Conexao.client()
        db = cliente['ocr_database']

        return [i for i in db.correcoes.find({"_id": ObjectId(_id)})]


    @staticmethod
    def listar(exibirImagem):
        cliente = Conexao.client()
        db = cliente['ocr_database']

        if exibirImagem:
            return db.correcoes.find({})
        else:
            return db.correcoes.find({}, { "imagens.saida": 0 })